import 'dart:async';
import 'dart:math';

import 'package:animate_do/animate_do.dart';
import 'package:better_player/src/controls/better_player_controls_configuration.dart';
import 'package:better_player/src/controls/better_player_cupertino_controls.dart';
import 'package:better_player/src/controls/better_player_material_controls.dart';
import 'package:better_player/src/core/better_player_controller.dart';
import 'package:better_player/src/core/better_player_utils.dart';
import 'package:better_player/src/subtitles/better_player_subtitles_configuration.dart';
import 'package:better_player/src/subtitles/better_player_subtitles_drawer.dart';
import 'package:better_player/src/video_player/video_player.dart';
import 'package:flutter/material.dart';
import 'package:better_player/src/core/globals.dart' as Globals;
import 'package:flutter/services.dart';

class BetterPlayerWithControls extends StatefulWidget {
  final BetterPlayerController controller;

  BetterPlayerWithControls({Key key, this.controller}) : super(key: key);

  @override
  _BetterPlayerWithControlsState createState() =>
      _BetterPlayerWithControlsState();
}

class _BetterPlayerWithControlsState extends State<BetterPlayerWithControls> {
  BetterPlayerSubtitlesConfiguration get subtitlesConfiguration =>
      widget.controller.betterPlayerConfiguration.subtitlesConfiguration;

  BetterPlayerControlsConfiguration get controlsConfiguration =>
      widget.controller.betterPlayerConfiguration.controlsConfiguration;

  final StreamController<bool> playerVisibilityStreamController =
      StreamController();

  bool showControlPipMode = true;

  @override
  void initState() {
    playerVisibilityStreamController.add(true);
    widget.controller.addListener(_onControllerChanged);
    super.initState();
  }

  @override
  void didUpdateWidget(BetterPlayerWithControls oldWidget) {
    if (oldWidget.controller != widget.controller) {
      widget.controller.addListener(_onControllerChanged);
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    playerVisibilityStreamController.close();
    widget.controller.removeListener(_onControllerChanged);
    super.dispose();
  }

  void _onControllerChanged() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final BetterPlayerController betterPlayerController =
        BetterPlayerController.of(context);

    return Center(
      child: Container(
        width: double.infinity,
        color: Colors.black,
        child: AspectRatio(
          aspectRatio: betterPlayerController.aspectRatio ??
              BetterPlayerUtils.calculateAspectRatio(context),
          child: _buildPlayerWithControls(betterPlayerController, context),
        ),
      ),
    );
  }

  Container _buildPlayerWithControls(
      BetterPlayerController betterPlayerController, BuildContext context) {
    var configuration = betterPlayerController.betterPlayerConfiguration;
    var rotation = configuration.rotation;

    Globals.betterPlayerController = betterPlayerController;

    if (!(rotation <= 360 && rotation % 90 == 0)) {
      print("Invalid rotation provided. Using rotation = 0");
      rotation = 0;
    }
    // print(betterPlayerController.betterPlayerConfiguration.instanceSharedState
    //     .getBool("InPipMode"));
    return showControlPipMode
        ? Container(
            decoration: BoxDecoration(color: Colors.black),
            child: Stack(
              fit: StackFit.passthrough,
              children: <Widget>[
                betterPlayerController.placeholder ?? Container(),
                BetterPlayerVideoFitWidget(
                  betterPlayerController,
                  betterPlayerController.betterPlayerConfiguration.fit,
                ),
                // Transform.rotate(
                //   angle: rotation * pi / 180,
                //   // child: testcategory(betterPlayerController),
                //   child: BetterPlayerVideoFitWidget(
                //     betterPlayerController,
                //     betterPlayerController.betterPlayerConfiguration.fit,
                //   ),
                // ),
                betterPlayerController.overlay ?? Container(),
                // BetterPlayerSubtitlesDrawer(
                //   betterPlayerController: betterPlayerController,
                //   betterPlayerSubtitlesConfiguration: subtitlesConfiguration,
                //   subtitles: betterPlayerController.subtitlesLines,
                //   playerVisibilityStream:
                //       playerVisibilityStreamController.stream,
                // ),
                _buildControls(context, betterPlayerController,
                    iconPipMode(betterPlayerController))
              ],
            ),
          )
        : Container();
  }

  Widget _buildControls(BuildContext context,
      BetterPlayerController betterPlayerController, Widget iconPipMpde) {
    return controlsConfiguration.showControls
        ? controlsConfiguration.customControls != null
            ? controlsConfiguration.customControls
            : BetterPlayerMaterialControls(
                onControlsVisibilityChanged: onControlsVisibilityChanged,
                controlsConfiguration: controlsConfiguration,
                iconPipMode: iconPipMpde,
              )
        : const SizedBox();
  }

  void onControlsVisibilityChanged(bool state) {
    playerVisibilityStreamController.add(state);
  }

  Widget iconPipMode(BetterPlayerController betterPlayerController) {
    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(100),
        child: Material(
          color: Colors.transparent,
          child: IconButton(
              color: Colors.white,
              icon: Icon(Icons.keyboard_arrow_down_rounded),
              onPressed: () {
                betterPlayerController
                    .betterPlayerConfiguration.instanceSharedState
                    .setBool("InPipMode", true);
                SystemChrome.setPreferredOrientations([
                  DeviceOrientation.portraitUp,
                  DeviceOrientation.portraitDown
                ]);
                // Globals.showControlPipMode = false;
                setState(() {
                  showControlPipMode = false;
                  Globals.showControlPipMode = false;
                });
                betterPlayerController
                    .betterPlayerConfiguration.contextForeing.currentState
                    .showBottomSheet<void>((contextSheet) {
                  return WillPopScope(
                      child: InkWell(
                        onTap: () {},
                        child: BottomSheet(
                            onClosing: () {},
                            enableDrag: true,
                            builder: (contextSheet) {
                              return Container(
                                  decoration:
                                      BoxDecoration(color: Colors.transparent),
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        width: MediaQuery.of(
                                                    betterPlayerController
                                                        .betterPlayerConfiguration
                                                        .contextPrincipalView)
                                                .size
                                                .width /
                                            3,
                                        color: Colors.black,
                                        constraints: BoxConstraints(
                                          maxWidth: MediaQuery.of(
                                                  betterPlayerController
                                                      .betterPlayerConfiguration
                                                      .contextPrincipalView)
                                              .size
                                              .width,
                                        ),
                                        child: AspectRatio(
                                          aspectRatio: 16 / 9,
                                          child: IgnorePointer(
                                            ignoring: true,
                                            child: BetterPlayerVideoFitWidget(
                                                Globals.betterPlayerController,
                                                Globals
                                                    .betterPlayerController
                                                    .betterPlayerConfiguration
                                                    .fit),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              Text(
                                                betterPlayerController
                                                    .betterPlayerConfiguration
                                                    .titleVideo,
                                                maxLines: 1,
                                                overflow: TextOverflow.ellipsis,
                                              ),
                                              Padding(
                                                padding:
                                                    EdgeInsets.only(top: 5),
                                                child: Text("Tv en Vivo",
                                                    style: TextStyle(
                                                        color:
                                                            Colors.grey[600])),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      IconButton(
                                        icon: Icon(Icons.close),
                                        onPressed: () {
                                          betterPlayerController
                                              .betterPlayerConfiguration
                                              .instanceSharedState
                                              .setBool("InPipMode", false);

                                          setState(() {
                                            showControlPipMode = true;
                                            Globals.showControlPipMode = true;
                                            if (Globals.portcentVisibility ==
                                                0) {
                                              betterPlayerController
                                                  .onPlayerVisibilityChanged(
                                                      Globals
                                                          .portcentVisibility,
                                                      showControlPipMode);
                                            }
                                          });

                                          Navigator.pop(betterPlayerController
                                              .betterPlayerConfiguration
                                              .contextPrincipalView);
                                          // _betterPlayerController
                                          //     .retryDataSource();
                                        },
                                      )
                                    ],
                                  ));
                            }),
                      ),
                      onWillPop: () async {
                        return await Future.value(true);
                      });
                });
              }),
        ),
      ),
    );
  }
}

///Widget used to set the proper box fit of the video. Default fit is 'fill'.
class BetterPlayerVideoFitWidget extends StatefulWidget {
  BetterPlayerVideoFitWidget(
    this.betterPlayerController,
    this.boxFit,
  )   : assert(betterPlayerController != null,
            "BetterPlayerController can't be null"),
        assert(boxFit != null, "BoxFit can't be null");

  final BetterPlayerController betterPlayerController;
  final BoxFit boxFit;

  @override
  _BetterPlayerVideoFitWidgetState createState() =>
      _BetterPlayerVideoFitWidgetState();
}

class _BetterPlayerVideoFitWidgetState
    extends State<BetterPlayerVideoFitWidget> {
  VideoPlayerController get controller =>
      widget.betterPlayerController.videoPlayerController;

  bool _initialized = false;

  VoidCallback _initializedListener;

  @override
  void initState() {
    super.initState();
    _initialize();
  }

  @override
  void didUpdateWidget(BetterPlayerVideoFitWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.betterPlayerController.videoPlayerController != controller) {
      oldWidget.betterPlayerController.videoPlayerController
          .removeListener(_initializedListener);
      _initialized = false;
      _initialize();
    }
  }

  void _initialize() {
    _initializedListener = () {
      if (!mounted) {
        return;
      }
      if (_initialized != controller.value.initialized) {
        _initialized = controller.value.initialized;
        setState(() {});
      }
    };
    controller.addListener(_initializedListener);
  }

  @override
  Widget build(BuildContext context) {
    if (_initialized) {
      return Center(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          child: FittedBox(
            fit: widget.boxFit ?? BoxFit.fill,
            child: SizedBox(
                width: controller.value.size?.width ?? 0,
                height: controller.value.size?.height ?? 0,
                child: VideoPlayer(controller)
                //
                ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }
}
