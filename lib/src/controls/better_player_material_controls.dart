import 'dart:async';
import 'dart:math';

import 'package:better_player/src/controls/better_player_clickable_widget.dart';
import 'package:better_player/src/controls/better_player_controls_configuration.dart';
import 'package:better_player/src/controls/better_player_controls_state.dart';
import 'package:better_player/src/controls/better_player_material_progress_bar.dart';
import 'package:better_player/src/controls/better_player_progress_colors.dart';
import 'package:better_player/src/core/better_player_controller.dart';
import 'package:better_player/src/core/better_player_utils.dart';
import 'package:better_player/src/core/better_player_with_controls.dart';
import 'package:better_player/src/video_player/video_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:better_player/src/core/globals.dart' as Globals;

import 'better_player_clickable_widget.dart';

class BetterPlayerMaterialControls extends StatefulWidget {
  ///Callback used to send information if player bar is hidden or not
  final Function(bool visbility) onControlsVisibilityChanged;

  ///Controls config
  ///
  final BetterPlayerControlsConfiguration controlsConfiguration;
  final Widget iconPipMode;

  BetterPlayerMaterialControls(
      {Key key,
      this.onControlsVisibilityChanged,
      this.controlsConfiguration,
      this.iconPipMode})
      : assert(onControlsVisibilityChanged != null),
        assert(controlsConfiguration != null),
        super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _BetterPlayerMaterialControlsState();
  }
}

class _BetterPlayerMaterialControlsState
    extends BetterPlayerControlsState<BetterPlayerMaterialControls> {
  VideoPlayerValue _latestValue;
  double _latestVolume;
  bool _hideStuff = true;
  Timer _hideTimer;
  Timer _initTimer;
  Timer _showAfterExpandCollapseTimer;
  bool _dragging = false;
  bool _displayTapped = false;
  VideoPlayerController _controller;
  BetterPlayerController _betterPlayerController;
  bool fullScreen = false;

  BetterPlayerControlsConfiguration get _controlsConfiguration =>
      widget.controlsConfiguration;

  @override
  Widget build(BuildContext context) {
    // if (_latestValue.hasError) {
    //   return _buildErrorWidget();
    // }

    return MouseRegion(
      onHover: (_) {
        _cancelAndRestartTimer();
      },
      child: Container(
        decoration: BoxDecoration(
            color: _hideStuff
                ? Colors.transparent
                : Colors.black.withOpacity(0.65)),
        child: GestureDetector(
          onTap: () {
            _hideStuff
                ? _cancelAndRestartTimer()
                : setState(() {
                    _hideStuff = true;
                  });
          },
          child: AbsorbPointer(
            absorbing: _hideStuff,
            child: Column(
              children: [
                _buildTopBar(),
                // isLoading(_latestValue)
                //     ? Expanded(child: Center(child: _buildLoadingWidget()))
                //     : _buildHitArea(),

                _buildHitArea(),
                if (_betterPlayerController.isLiveStream() == false)
                  buildBottomItemsExtended(),
                _betterPlayerController.isLiveStream() == false
                    ? _buildBottomBar()
                    : _buildBottomBarLiveStream(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _dispose();
    super.dispose();
  }

  void _dispose() {
    _controller?.removeListener(_updateState);
    _hideTimer?.cancel();
    _initTimer?.cancel();
    _showAfterExpandCollapseTimer?.cancel();
  }

  @override
  void didChangeDependencies() {
    final _oldController = _betterPlayerController;
    _betterPlayerController = BetterPlayerController.of(context);
    _controller = _betterPlayerController.videoPlayerController;

    if (_oldController != _betterPlayerController) {
      _dispose();
      _initialize();
    }

    super.didChangeDependencies();
  }

  Widget _buildErrorWidget() {
    if (_betterPlayerController.errorBuilder != null) {
      return _betterPlayerController.errorBuilder(context,
          _betterPlayerController.videoPlayerController.value.errorDescription);
    } else {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.warning,
              color: _controlsConfiguration.iconsColor,
              size: 42,
            ),
            Text(
              _controlsConfiguration.defaultErrorText,
              style: TextStyle(color: _controlsConfiguration.textColor),
            ),
          ],
        ),
      );
    }
  }

  AnimatedOpacity _buildTopBar() {
    Size size = MediaQuery.of(context).size;

    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 1.0,
      duration: _controlsConfiguration.controlsHideTime,
      onEnd: _onPlayerHide,
      child: Container(
          height: _controlsConfiguration.controlBarHeight,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _betterPlayerController
                              .betterPlayerConfiguration.showCloseButton !=
                          null &&
                      _betterPlayerController
                              .betterPlayerConfiguration.showCloseButton ==
                          true
                  ? Container(
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(100),
                        child: Material(
                          color: Colors.transparent,
                          child: IconButton(
                              icon: Icon(
                                Icons.close,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                Navigator.of(context).pop();
                              }),
                        ),
                      ),
                    )
                  : _betterPlayerController.betterPlayerConfiguration
                                  .showOverlayButton !=
                              null &&
                          _betterPlayerController.betterPlayerConfiguration
                                  .showOverlayButton ==
                              true
                      ? _latestValue != null ||
                              _latestValue.position != null ||
                              _latestValue.duration != null ||
                              !_latestValue.hasError
                          ? MediaQuery.of(context).orientation ==
                                  Orientation.landscape
                              ? Container()
                              : Container(
                                  child: widget.iconPipMode,
                                )
                          : Container()
                      : Container(),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 8),
                width: size.width / 2,
                child: Center(
                  child: Text(
                      _betterPlayerController
                                      .betterPlayerConfiguration.titleVideo ==
                                  null ||
                              _betterPlayerController.betterPlayerConfiguration
                                      .titleVideo.length ==
                                  0
                          ? ""
                          : _betterPlayerController
                              .betterPlayerConfiguration.titleVideo,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: MediaQuery.of(context).orientation ==
                                Orientation.landscape
                            ? 22
                            : 17,
                      )),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [_buildMuteButton(_controller), _buildMoreButton()],
              ),
            ],
          )),
    );
  }

  Widget _buildMoreButton() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(100),
      child: Material(
        color: Colors.transparent,
        child: AnimatedOpacity(
          opacity: _hideStuff ? 0.0 : 1.0,
          duration: _controlsConfiguration.controlsHideTime,
          child: IconButton(
            onPressed: () {
              onShowMoreClicked();
            },
            icon: Icon(
              Icons.more_vert_rounded,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
    // return BetterPlayerMaterialClickableWidget(
    //   child: Padding(
    //     padding: const EdgeInsets.all(8),
    //     child: Icon(
    //       Icons.more_vert,
    //       color: Colors.white,
    //     ),
    //   ),
    //   onTap: () {
    //     onShowMoreClicked();
    //   },
    // );
  }

  AnimatedOpacity _buildBottomBar() {
    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 1.0,
      duration: _controlsConfiguration.controlsHideTime,
      onEnd: _onPlayerHide,
      child: Container(
        decoration: BoxDecoration(color: Colors.transparent),
        height: 25,
        child: Row(
          children: [
            // _controlsConfiguration.enablePlayPause
            //     ? _buildPlayPause(_controller)
            //     : const SizedBox(),
            // _betterPlayerController.isLiveStream()
            //     ? _buildLiveWidget()
            //     : _controlsConfiguration.enableProgressText
            //         ? _buildPosition()
            //         : const SizedBox(),
            _betterPlayerController.isLiveStream()
                ? const SizedBox()
                : _controlsConfiguration.enableProgressBar
                    ? _buildProgressBar()
                    : const SizedBox(),
            // _controlsConfiguration.enableMute
            //     ? _buildMuteButton(_controller)
            //     : const SizedBox(),
            // _controlsConfiguration.enableFullscreen
            //     ? _buildExpandButton()
            //     : const SizedBox(),
          ],
        ),
      ),
    );
  }

  AnimatedOpacity _buildBottomBarLiveStream() {
    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 1.0,
      duration: _controlsConfiguration.controlsHideTime,
      onEnd: _onPlayerHide,
      child: Container(
        decoration: BoxDecoration(color: Colors.transparent),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            // _controlsConfiguration.enablePlayPause
            //     ? _buildPlayPause(_controller)
            //     : const SizedBox(),
            // _betterPlayerController.isLiveStream()
            //     ? _buildLiveWidget()
            //     : _controlsConfiguration.enableProgressText
            //         ? _buildPosition()
            //         : const SizedBox(),
            // _betterPlayerController.isLiveStream()
            //     ? const SizedBox()
            //     : _controlsConfiguration.enableProgressBar
            //         ? _buildProgressBar()
            //         : const SizedBox(),
            // _controlsConfiguration.enableMute
            //     ? _buildMuteButton(_controller)
            //     : const SizedBox(),
            _controlsConfiguration.enableFullscreen
                ? _buildExpandButton()
                : const SizedBox(),
          ],
        ),
      ),
    );
  }

  Widget _buildLiveWidget() {
    return Expanded(
      child: Text(
        _controlsConfiguration.liveText,
        style: TextStyle(
            color: _controlsConfiguration.liveTextColor,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _buildExpandButton() {
    return AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: _controlsConfiguration.controlsHideTime,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(100),
          child: Material(
            color: Colors.transparent,
            child: IconButton(
              onPressed: _onExpandCollapse,
              icon: Icon(
                MediaQuery.of(context).orientation == Orientation.landscape
                    ? Icons.fullscreen_exit_rounded
                    : Icons.fullscreen_rounded,
                color: Colors.white,
              ),
            ),
          ),
        )
        // child: Container(
        //   height: _controlsConfiguration.controlBarHeight,
        //   // margin: EdgeInsets.only(right: 12.0),
        //   padding: EdgeInsets.symmetric(horizontal: 20.0),
        //   child: Center(
        //     child: Icon(
        //       _betterPlayerController.isFullScreen
        //           ? Icons.fullscreen_exit
        //           : Icons.fullscreen,
        //       color: _controlsConfiguration.iconsColor,
        //     ),
        //   ),
        // ),

        );
  }

  Widget _buildHitArea() {
    return Expanded(
      child: Container(
        color: Colors.transparent,
        child: Center(
            child: Stack(
          children: [
            _latestValue.hasError
                ? _buildResetVideo()
                : _betterPlayerController.isLiveStream()
                    ? isLoading(_latestValue)
                        ? Center(child: _buildLoadingWidget())
                        : Container()
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          _latestValue == null ||
                                  _latestValue.position == null ||
                                  _latestValue.duration == null
                              ? Container()
                              : _buildSkipButton(),
                          isLoading(_latestValue)
                              ? Center(child: _buildLoadingWidget())
                              : _buildPlayPause(_controller),
                          _latestValue == null ||
                                  _latestValue.position == null ||
                                  _latestValue.duration == null
                              ? Container()
                              : _buildForwardButton(),
                          // _buildNextVideoWidget(),
                        ],
                      ),
            // : _buildPlayReplayButton(),

            // _buildNextVideoWidget(),
          ],
        )),
      ),
    );
  }

  Widget _buildResetVideo() {
    Size iconsSIZE = MediaQuery.of(context).size;
    Orientation deviceView = MediaQuery.of(context).orientation;
    return AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: _controlsConfiguration.controlsHideTime,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(100),
          child: Material(
            color: Colors.transparent,
            child: IconButton(
              iconSize: deviceView == Orientation.landscape
                  ? iconsSIZE.height / 6.5
                  : iconsSIZE.height / 15,
              onPressed: () {
                _betterPlayerController.retryDataSource();
              },
              icon: Icon(
                Icons.replay_rounded,
                color: Colors.white,
              ),
            ),
          ),
        ));
  }

  Widget _buildSkipButton() {
    Size iconsSIZE = MediaQuery.of(context).size;
    Orientation deviceView = MediaQuery.of(context).orientation;

    return AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: _controlsConfiguration.controlsHideTime,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(100),
          child: Material(
            color: Colors.transparent,
            child: IconButton(
              onPressed: skipBack,
              iconSize: deviceView == Orientation.landscape
                  ? iconsSIZE.height / 6.5
                  : iconsSIZE.height / 15,
              icon: Icon(
                Icons.replay_10_rounded,
                color: Colors.white,
              ),
            ),
          ),
        ));
  }

  void skipBack() {
    _cancelAndRestartTimer();
    final beginning = const Duration().inMilliseconds;
    final skip =
        (_latestValue.position - Duration(milliseconds: 10000)).inMilliseconds;
    _betterPlayerController
        .seekTo(Duration(milliseconds: max(skip, beginning)));
  }

  Widget _buildForwardButton() {
    Size iconsSIZE = MediaQuery.of(context).size;
    Orientation deviceView = MediaQuery.of(context).orientation;

    return AnimatedOpacity(
        opacity: _hideStuff ? 0.0 : 1.0,
        duration: _controlsConfiguration.controlsHideTime,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(100),
          child: Material(
            color: Colors.transparent,
            child: IconButton(
              onPressed: skipForward,
              iconSize: deviceView == Orientation.landscape
                  ? iconsSIZE.height / 6.5
                  : iconsSIZE.height / 15,
              icon: Icon(
                Icons.forward_10_rounded,
                color: Colors.white,
              ),
            ),
          ),
        ));
  }

  void skipForward() {
    _cancelAndRestartTimer();
    final end = _latestValue.duration.inMilliseconds;
    final skip =
        (_latestValue.position + Duration(milliseconds: 10000)).inMilliseconds;
    _betterPlayerController.seekTo(Duration(milliseconds: min(skip, end)));
  }

  Widget buildBottomItemsExtended() {
    return AnimatedOpacity(
      opacity: _hideStuff ? 0 : 1,
      duration: _controlsConfiguration.controlsHideTime,
      child: GestureDetector(
        onTap: () {
          setState(() {
            _hideStuff = true;
          });
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _buildPosition(),
            _controlsConfiguration.enableFullscreen
                ? _buildExpandButton()
                : Container()
          ],
        ),
      ),
    );
  }

  Widget _buildPlayReplayButton() {
    // bool isFinished = _latestValue?.position != null &&
    //     _latestValue?.duration != null &&
    //     _latestValue.position >= _latestValue.duration;
    // IconData _hitAreaIconData = isFinished ? Icons.replay : Icons.play_arrow;
    Size iconsSIZE = MediaQuery.of(context).size;
    Orientation deviceView = MediaQuery.of(context).orientation;

    return IconButton(
        color: Colors.white,
        iconSize: deviceView == Orientation.landscape
            ? iconsSIZE.height / 6.5
            : iconsSIZE.height / 8,
        icon: Icon(Icons.replay_rounded),
        onPressed: () {
          if (_latestValue != null && _latestValue.isPlaying) {
            if (_displayTapped) {
              setState(() {
                _hideStuff = true;
              });
            } else
              _cancelAndRestartTimer();
          } else {
            _onPlayPause();

            setState(() {
              _hideStuff = true;
            });
          }
        });
    // return BetterPlayerMaterialClickableWidget(
    //   child: Align(
    //     alignment: Alignment.center,
    //     child: Container(
    //       child: Padding(
    //         padding: const EdgeInsets.all(12),
    //         child: Stack(
    //           children: [
    //             Icon(
    //               _hitAreaIconData,
    //               size: 32,
    //               color: _controlsConfiguration.iconsColor,
    //             )
    //           ],
    //         ),
    //       ),
    //     ),
    //   ),
    //   onTap: () {
    //     if (_latestValue != null && _latestValue.isPlaying) {
    //       if (_displayTapped) {
    //         setState(() {
    //           _hideStuff = true;
    //         });
    //       } else
    //         _cancelAndRestartTimer();
    //     } else {
    //       _onPlayPause();

    //       setState(() {
    //         _hideStuff = true;
    //       });
    //     }
    //   },
    // );
  }

  Widget _buildNextVideoWidget() {
    return StreamBuilder<int>(
      stream: _betterPlayerController.nextVideoTimeStreamController.stream,
      builder: (context, snapshot) {
        if (snapshot.data != null) {
          return BetterPlayerMaterialClickableWidget(
            onTap: () {
              _betterPlayerController.playNextVideo();
            },
            child: Align(
              alignment: Alignment.bottomRight,
              child: Container(
                margin: const EdgeInsets.only(bottom: 4, right: 4),
                decoration: BoxDecoration(
                  color: _controlsConfiguration.controlBarColor,
                  borderRadius: BorderRadius.circular(48),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(12),
                  child: Text(
                    "siguiente video en ${snapshot.data} ...",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
          );
        } else {
          return const SizedBox();
        }
      },
    );
  }

  Widget _buildMuteButton(
    VideoPlayerController controller,
  ) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(100),
      child: Material(
        color: Colors.transparent,
        child: AnimatedOpacity(
          opacity: _hideStuff ? 0.0 : 1.0,
          duration: _controlsConfiguration.controlsHideTime,
          child: IconButton(
            onPressed: () {
              _cancelAndRestartTimer();
              if (_latestValue.volume == 0) {
                _betterPlayerController.setVolume(_latestVolume ?? 0.5);
              } else {
                _latestVolume = controller.value.volume;
                _betterPlayerController.setVolume(0.0);
              }
            },
            icon: Icon(
              (_latestValue != null && _latestValue.volume > 0)
                  ? Icons.volume_up_rounded
                  : Icons.volume_off_rounded,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
    // return BetterPlayerMaterialClickableWidget(
    //   onTap: () {
    //     _cancelAndRestartTimer();
    //     if (_latestValue.volume == 0) {
    //       _betterPlayerController.setVolume(_latestVolume ?? 0.5);
    //     } else {
    //       _latestVolume = controller.value.volume;
    //       _betterPlayerController.setVolume(0.0);
    //     }
    //   },
    //   child: AnimatedOpacity(
    //     opacity: _hideStuff ? 0.0 : 1.0,
    //     duration: _controlsConfiguration.controlsHideTime,
    //     child: ClipRect(
    //       child: Container(
    //         child: Container(
    //           height: _controlsConfiguration.controlBarHeight,
    //           padding: EdgeInsets.symmetric(horizontal: 8),
    //           child: Icon(
    //             (_latestValue != null && _latestValue.volume > 0)
    //                 ? _controlsConfiguration.muteIcon
    //                 : _controlsConfiguration.unMuteIcon,
    //             color: _controlsConfiguration.iconsColor,
    //           ),
    //         ),
    //       ),
    //     ),
    //   ),
    // );
  }

  Widget _buildPlayPause(VideoPlayerController controller) {
    Size iconsSIZE = MediaQuery.of(context).size;
    Orientation deviceView = MediaQuery.of(context).orientation;
    final bool isFinished = isVideoFinished(_latestValue);

    return AnimatedOpacity(
      opacity: _hideStuff ? 0.0 : 1.0,
      duration: _controlsConfiguration.controlsHideTime,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(50),
        child: Material(
          color: Colors.transparent,
          child: isFinished
              ? _buildPlayReplayButton()
              : IconButton(
                  iconSize: deviceView == Orientation.landscape
                      ? iconsSIZE.height / 6.5
                      : iconsSIZE.height / 15,
                  icon: Icon(
                    controller.value.isPlaying
                        ? Icons.pause_rounded
                        : Icons.play_arrow_rounded,
                    color: Colors.white,
                  ),
                  onPressed: _onPlayPause),
        ),
      ),
    );
    // return BetterPlayerMaterialClickableWidget(
    //   onTap: _onPlayPause,
    //   child: Container(
    //     height: _controlsConfiguration.controlBarHeight,
    //     margin: const EdgeInsets.symmetric(horizontal: 4),
    //     padding: const EdgeInsets.symmetric(horizontal: 12),
    //     child: Icon(
    //       controller.value.isPlaying
    //           ? _controlsConfiguration.pauseIcon
    //           : _controlsConfiguration.playIcon,
    //       color: _controlsConfiguration.iconsColor,
    //     ),
    //   ),
    // );
  }

  Widget _buildPosition() {
    final position = _latestValue != null && _latestValue.position != null
        ? _latestValue.position
        : Duration.zero;
    final duration = _latestValue != null && _latestValue.duration != null
        ? _latestValue.duration
        : Duration.zero;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: _latestValue == null ||
              _latestValue.position == null ||
              _latestValue.duration == null
          ? Text(
              '',
              style: TextStyle(
                color: Colors.white,
                fontSize: 14.0,
              ),
            )
          : Text(
              '${BetterPlayerUtils.formatDuration(position)} / ${BetterPlayerUtils.formatDuration(duration)}',
              style: TextStyle(
                fontSize: 14,
                color: _controlsConfiguration.textColor,
              ),
            ),
    );
  }

  void _cancelAndRestartTimer() {
    _hideTimer?.cancel();
    _startHideTimer();

    setState(() {
      _hideStuff = false;
      _displayTapped = true;
    });
  }

  Future<Null> _initialize() async {
    _controller.addListener(_updateState);

    _updateState();

    if ((_controller.value != null && _controller.value.isPlaying) ||
        _betterPlayerController.autoPlay) {
      _startHideTimer();
    }

    if (_controlsConfiguration.showControlsOnInitialize) {
      _initTimer = Timer(Duration(milliseconds: 200), () {
        setState(() {
          _hideStuff = false;
        });
      });
    }
  }

  void _onExpandCollapse() {
    if (fullScreen == false) {
      setState(() {
        fullScreen = true;
        _hideStuff = true;

        SystemChrome.setPreferredOrientations([
          DeviceOrientation.landscapeLeft,
          DeviceOrientation.landscapeRight
        ]);
        SystemChrome.setEnabledSystemUIOverlays([]);
      });
    } else if (fullScreen) {
      setState(() {
        fullScreen = false;
        SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
        ]);
        SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
      });
    }
    // setState(() {
    //   _hideStuff = true;

    //   _betterPlayerController.toggleFullScreen();
    //   _showAfterExpandCollapseTimer =
    //       Timer(_controlsConfiguration.controlsHideTime, () {
    //     setState(() {
    //       _cancelAndRestartTimer();
    //     });
    //   });
    // });
  }

  void _onPlayPause() {
    bool isFinished = false;

    if (_latestValue?.position != null && _latestValue?.duration != null) {
      isFinished = _latestValue.position >= _latestValue.duration;
    }

    setState(() {
      if (_controller.value.isPlaying) {
        _hideStuff = false;
        _hideTimer?.cancel();
        _betterPlayerController.pause();
      } else {
        _cancelAndRestartTimer();

        if (!_controller.value.initialized) {
        } else {
          if (isFinished) {
            _betterPlayerController.seekTo(Duration(seconds: 0));
          }
          _betterPlayerController.play();
          _betterPlayerController.cancelNextVideoTimer();
        }
      }
    });
  }

  void _startHideTimer() {
    _hideTimer = Timer(const Duration(seconds: 2), () {
      setState(() {
        _hideStuff = true;
      });
    });
  }

  void _updateState() {
    if (this.mounted) {
      setState(() {
        _latestValue = _controller.value;
        if (_latestValue != null) {
          if (_latestValue.hasError) {
            _hideStuff = false;
          }
          if (_latestValue.duration != null) {
            if (_latestValue.position >= _latestValue.duration) {
              _hideStuff = false;
            }
          }
        }
      });
    }
  }

  Widget _buildProgressBar() {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: BetterPlayerMaterialVideoProgressBar(
          _controller,
          _betterPlayerController,
          onDragStart: () {
            setState(() {
              _dragging = true;
            });
            _hideTimer?.cancel();
          },
          onDragEnd: () {
            setState(() {
              _dragging = false;
            });
            _startHideTimer();
          },
          colors: BetterPlayerProgressColors(
              playedColor: Theme.of(context).accentColor,
              handleColor: Theme.of(context).accentColor,
              bufferedColor: Colors.white30,
              backgroundColor: Colors.white30),
        ),
      ),
    );
  }

  void _onPlayerHide() {
    _betterPlayerController.toggleControlsVisibility(!_hideStuff);
    widget.onControlsVisibilityChanged(!_hideStuff);
  }

  Widget _buildLoadingWidget() {
    Size iconsSIZE = MediaQuery.of(context).size;
    Orientation deviceView = MediaQuery.of(context).orientation;
    return SizedBox(
      height: deviceView == Orientation.landscape
          ? iconsSIZE.height / 6.5
          : iconsSIZE.height / 15,
      width: deviceView == Orientation.landscape
          ? iconsSIZE.height / 6.5
          : iconsSIZE.height / 15,
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      ),
    );
  }

  @override
  BetterPlayerController getBetterPlayerController() => _betterPlayerController;
}
